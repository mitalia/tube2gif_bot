#!/usr/bin/env python2
import subprocess as sp
import os
import os.path
import sys
import tempfile

def video2gif(src, out, skip_seconds, duration, remote, subs_file = None, width=320, fps=10):
    subs_filter = ''
    fixed_subs_file = None
    if subs_file:
        import re
        import tempfile
        f,fixed_subs_file = tempfile.mkstemp(suffix=os.path.splitext(subs_file)[1])
        os.close(f)
        sp.check_call(['ffmpeg', '-y', '-i', subs_file, '-ss', str(skip_seconds), fixed_subs_file])
        subs_filter='subtitles=' + re.sub(r"[:'\[\],;\\]", r'\\\g<0>', fixed_subs_file) + ":force_style='Fontsize=36',"
    try:
        if remote:
            urls = sp.check_output(['youtube-dl', '--get-url',
                '-f', 'best[width>=%d]' % (width,), src]).splitlines()
            if len(urls)==0:
                raise RuntimeError("youtube-dl provided no results")
            src = urls[0]
        sp.check_call([
            'ffmpeg', '-y', '-ss', str(skip_seconds),
            '-t', str(duration),
            '-i', src,
            '-filter_complex',
            subs_filter + 'fps=%d,scale=%d:-1:flags=lanczos,split=2 [a][b]; [a] palettegen [pal]; [b] fifo [b]; [b] [pal] paletteuse' % (fps, width),
            out])
    finally:
        if fixed_subs_file:
            os.unlink(fixed_subs_file)


if __name__=='__main__':
    if not (5 <= len(sys.argv) <= 6):
        print "Usage mkgif (input.mp4|youtube link) output.gif skip_seconds duration [sub_file]"
        sys.exit(1)

    src = sys.argv[1]
    out = sys.argv[2]
    skip_seconds = float(sys.argv[3])
    duration = float(sys.argv[4])
    remote = "youtube.com" in src or "youtu.be" in src
    subs_file = None
    if len(sys.argv) > 5:
        subs_file = sys.argv[5]
    video2gif(src, out, skip_seconds, duration, remote=remote, subs_file = subs_file)

