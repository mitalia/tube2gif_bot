#!/usr/bin/env python2
import urlparse
import sys
import telepot
import random
from telepot.delegate import per_chat_id, create_open
import re
import threading
import json
import signal
import atexit
import mkgif
import tempfile
import os
from collections import defaultdict
import copy

class StatsHandler:
    def __init__(self, logfile):
        self.lock = threading.RLock()
        self.fd = open(logfile, "a")

    def do_log(self, *args):
        with self.lock:
            self.fd.write('\t'.join(str(x) for x in args) + "\n")
            self.fd.flush()

    def gif_created(self, *args):
        self.do_log("gif_created", *args)

    def gif_creation_error(self, *args):
        self.do_log("gif_creation_error", *args)

    def close(self):
        with self.lock:
            self.fd.close()
            self.fd = None

def parse_timestamp(s):
    m = re.match(r"^(?:(?:(?P<hours>\d+):)?(?P<minutes>\d+):)?(?P<seconds>\d+(?:[.]\d+)?)$", s)
    if not m:
        return None
    hours = m.group("hours") or "0"
    minutes = m.group("minutes") or "0"
    seconds = m.group("seconds")
    print repr(hours), repr(minutes), repr(seconds)
    return float(hours)*3600+float(minutes)*60+float(seconds)

class Tube2GifResponder(telepot.helper.ChatHandler):
    youtube_re = re.compile(r"(?:https?://|//)?(?:www\.|m\.)?(?:youtu\.be/|youtube\.com/)[^\s]*")
    STATE_DEFAULT = 0
    STATE_WAIT_FOR_SKIP = 1
    STATE_WAIT_FOR_DURATION = 2

    def __init__(self, seed_tuple, timeout):
        super(Tube2GifResponder, self).__init__(seed_tuple, timeout)
        self.state = self.STATE_DEFAULT
        self.url = ''
        self.skip = -1
        self.duration = -1

    def on_message(self, msg):
        send = self.sender.sendMessage
        content_type, chat_type, chat_id = telepot.glance2(msg)
        if content_type!="text":
            return
        txt = msg.get('text', '').strip()
        # default processing (stuff that makes us go back to default state)
        # YouTube URL
        yt = self.youtube_re.search(txt)
        if yt:
            self.url = yt.group(0)
            send(u"Great! Now tell me how many seconds \u23f2 you want to skip (plain seconds or hh:mm:ss are allowed)!")
            self.state = self.STATE_WAIT_FOR_SKIP
        elif self.state == self.STATE_DEFAULT or txt=='/help' or txt=='/start':
            # Default state, but we got something we didn't understand
            if txt!='/help' and txt!='/start':
                send(u"I can't seem to find YouTube links in your message \U0001F61E")
            send(u"Send me a link to a YouTube video \U0001F39E to make a GIF from it!")
            self.state = self.STATE_DEFAULT
        elif self.state == self.STATE_WAIT_FOR_SKIP:
            self.skip = parse_timestamp(txt)
            if self.skip is None:
                send(u"This doesn't look like a time... \U0001F615")
                return
            if self.skip < 0 or self.skip>365*86400L:
                send(u"Your number seems a bit off... \U0001F615")
                return
            send(u"All right, how many seconds will this last? (1 to 20) \u23f0")
            self.state = self.STATE_WAIT_FOR_DURATION
        elif self.state == self.STATE_WAIT_FOR_DURATION:
            self.duration = parse_timestamp(txt)
            if self.duration==None:
                send(u"This doesn't look like a duration... \U0001F615")
                return
            if self.duration < 1 or self.duration > 20:
                send(u"You can make GIFs from 1 to 20 seconds long \U0001F634")
                return
            send(u"Great! Hold tight while we bake your GIF! \U0001F382")
            self.state = self.STATE_DEFAULT
            fd,gif=tempfile.mkstemp(".gif", prefix="tube2gif")
            os.close(fd)
            try:
                mkgif.video2gif(self.url, gif, self.skip, self.duration, remote=True)
            except Exception as ex:
                send(u"Woops... looks like something went wrong! \U0001F61E")
                stats.gif_creation_error(self.url, self.skip, self.duration, gif, repr(ex))
                os.unlink(gif)
                return
            with open(gif, "rb") as fd:
                self.sender.sendDocument(fd)
            send(u"Heya \U0001F604! Here's your GIF!")
            stats.gif_created(self.url, self.skip, self.duration, gif)

if __name__=='__main__':
    TOKEN = sys.argv[1]  # get token from command-line
    LOG_FILE = sys.argv[2]
    stats = StatsHandler(LOG_FILE)

    bot = telepot.DelegatorBot(TOKEN, [
        (per_chat_id(), create_open(Tube2GifResponder, timeout=240)),
    ])
    print "We are up!"
    bot.notifyOnMessage(run_forever=True)
