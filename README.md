# tube2gif_bot #

A Telegram bot which uses `youtube_dl` and `ffmpeg` to extract the required portion of a video, convert it to a GIF and send it back. The "official" instance listens on `@tube2gif_bot`.

Notice that `mkgif.py` is not tied to Telegram at all, and can actually be used from the command line to create GIFs even for local files.

The conversion process is performed in four steps:

- the remote URL is found using `youtube_dl`;
- only the relevant portion of the *video* is retrieved to a local `mkv` file through `ffmpeg` (`youtube_dl` doesn't support partial downloads);
- the optimal palette for the target is calculated from the file above;
- finally, the `mkv` is converted to GIF (at a fixed size/frame rate) using the palette.

## License ##

Standard three-clause BSD; see LICENSE.